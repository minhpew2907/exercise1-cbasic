﻿const int SO_QUANG_VANG_THU_NHAT = 10;
const int GIA_QUANG_VANG_THU_NHAT = 10;
const int SO_QUANG_VANG_THU_HAI = 5;
const int GIA_QUANG_VANG_THU_HAI = 5;
const int SO_QUANG_VANG_THU_BA = 3;
const int GIA_QUANG_VANG_THU_BA = 2;
const int GIA_QUANG_VANG_CON_LAI = 1;


Console.WriteLine("Nhap so quang vang ban thu duoc: ");
int vangNhapVao = int.Parse(Console.ReadLine());

int vang1 = Math.Min(vangNhapVao, SO_QUANG_VANG_THU_NHAT);
int gia10QuangVang = vang1 * GIA_QUANG_VANG_THU_NHAT;
vangNhapVao -= vang1;

int vang2 = Math.Min(vangNhapVao, SO_QUANG_VANG_THU_HAI);
int gia5QuangVang = vang2 * GIA_QUANG_VANG_THU_HAI;
vangNhapVao -= vang2;

int vang3 = Math.Min(vangNhapVao, SO_QUANG_VANG_THU_BA);
int gia3QuangVang = vang3 * GIA_QUANG_VANG_THU_BA;
vangNhapVao -= vang3;

int giaQuangVangConLai = vangNhapVao * GIA_QUANG_VANG_CON_LAI;

int tongGiatien = (gia10QuangVang + gia5QuangVang + gia3QuangVang + giaQuangVangConLai);
Console.WriteLine("Tong so tien ban nhan duoc la: " + tongGiatien);
