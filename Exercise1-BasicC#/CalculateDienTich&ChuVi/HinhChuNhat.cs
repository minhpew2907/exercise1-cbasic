﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDienTich_ChuVi
{
    public class HinhChuNhat
    {
        public int DienTich(int a, int b)
        {
            return a * b;
        }

        public int ChuVi(int a, int b)
        {
            return 2 * (a + b);
        }
    }
}
